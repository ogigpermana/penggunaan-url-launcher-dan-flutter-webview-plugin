import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

const URL = 'https://detik.com/';

String url = "https://kompas.com/"; 

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Webview Example',
    theme: ThemeData.dark(),
    // Start the app with the "/" named route. In our case, the app will start
    // on the FirstScreen Widget
    initialRoute: '/',
    routes: {
      // When we navigate to the "/" route, build the FirstScreen Widget
      '/': (context) => FirstScreen(),
      // When we navigate to the "/second" route, build the SecondScreen Widget
      '/second': (context) => SecondScreen(),
      '/webview': (context) => WebviewScaffold(
        url: url,
        appBar: AppBar(
          actions: <Widget>[
            new IconButton(
             icon: new Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop(null),
           ),
          ],
          title: Text(url),
          automaticallyImplyLeading: false,
          leading: new Container(),
        ),
        hidden: true,
        withJavascript: true,
        withLocalStorage: true,
        withZoom: true,
      ),
    },
  ));
}

class FirstScreen extends StatelessWidget {

  Future launcherURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: true);
    } else {
      print("Tidak bisa membuka $url");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('URL Launcher Example'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(URL),
            ),
            RaisedButton(
              child: Text("Buka link"),
              onPressed: (){
                launcherURL(URL);
              },
            ),
            RaisedButton(
              child: Text('Ke halaman webview'),
              onPressed: () {
                // Navigate to the second screen using a named route
                Navigator.pushNamed(context, '/second');
              },
            ),
          ],
        ),
      ),
    );
  }
}

class SecondScreen extends StatefulWidget {
  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  final webView = FlutterWebviewPlugin();
  TextEditingController controller = TextEditingController(text: url);

  @override
    void initState() {
      super.initState();
      webView.close();
      controller.addListener(() {
        url = controller.text;
      });
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Webview Example"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0),
              child: TextField(
                controller: controller,
              ),
            ),
            RaisedButton(
              child: Text("Buka Link"),
              onPressed: (){
                Navigator.of(context).pushNamed("/webview");
              },
            )
          ],
        ),
      ),
    );
  }
}

